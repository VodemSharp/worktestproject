﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using WorkTestProject.Data.DbModels.Interfaces;

namespace WorkTestProject.Data.Repository
{
    public interface IRepository<T> where T : class, IDbEntity
    {
        IOrderedQueryable<T> ObjectSort(IQueryable<T> entities, Expression<Func<T, object>> expression, bool ASC = true);
        T GetEntityById(int id);
        T GetFirstEntity(Expression<Func<T, bool>> where = null, List<string> include = null, Expression<Func<T, object>> orderBy = null, bool ASC = true);
        IQueryable<T> GetEntities(Expression<Func<T, bool>> where = null, List<string> include = null, Expression<Func<T, object>> orderBy = null, bool ASC = true, int skip = -1, int take = -1);
        int CountEntities(Expression<Func<T, bool>> where = null, List<string> include = null);
        bool AnyEntity(Expression<Func<T, bool>> expression, List<string> include = null);
        T AddEntity(T entity);
        void UpdateEntity(T entity, List<string> editedFields);
        void RemoveEntity(int entityId);
        void AddRangeEntities(IEnumerable<T> entities);
        void RemoveRangeEntities(IEnumerable<T> entities);
    }
}
