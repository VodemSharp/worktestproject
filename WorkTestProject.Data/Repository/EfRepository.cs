﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using WorkTestProject.Data.DbModels.Interfaces;
using WorkTestProject.Data.Helpers;

namespace WorkTestProject.Data.Repository
{
    public class EfRepository<T> : IRepository<T> where T : class, IDbEntity
    {
        #region Fields

        private IDbContext db;
        private DbSet<T> dbSet;

        #endregion

        #region Ctor

        public EfRepository(IDbContext dbContext)
        {
            db = dbContext;
            dbSet = db.Set<T>();
        }

        #endregion

        #region Helpers

        public IOrderedQueryable<T> ObjectSort(IQueryable<T> entities, Expression<Func<T, object>> expression, bool ASC = true)
        {
            var unaryExpression = expression.Body as UnaryExpression;
            if (unaryExpression != null)
            {
                var propertyExpression = (MemberExpression)unaryExpression.Operand;
                var parameters = expression.Parameters;

                if (propertyExpression.Type == typeof(DateTime))
                {
                    var newExpression = Expression.Lambda<Func<T, DateTime>>(propertyExpression, parameters);
                    return ASC == true ? entities.OrderBy(newExpression) : entities.OrderByDescending(newExpression);
                }

                if (propertyExpression.Type == typeof(int))
                {
                    var newExpression = Expression.Lambda<Func<T, int>>(propertyExpression, parameters);
                    return ASC == true ? entities.OrderBy(newExpression) : entities.OrderByDescending(newExpression);
                }
            }

            return ASC == true ? entities.OrderBy(expression) : entities.OrderByDescending(expression);
        }

        #endregion

        #region Methods

        public T GetEntityById(int id)
        {
            return dbSet.FirstOrDefault(e => e.Id == id);
        }

        public T GetFirstEntity(Expression<Func<T, bool>> where = null, List<string> include = null, Expression<Func<T, object>> orderBy = null, bool ASC = true)
        {
            if (where == null)
            {
                if (include == null || include.Count == 0)
                {
                    if (orderBy == null)
                    {
                        return dbSet.FirstOrDefault();
                    }
                    else
                    {
                        return ObjectSort(dbSet, orderBy, ASC).FirstOrDefault();
                    }
                }
                else
                {
                    IQueryable<T> query = dbSet.Include(include[0]);

                    for (int i = 1; i < include.Count; i++)
                    {
                        query = query.Include(include[i]);
                    }

                    if (orderBy == null)
                    {
                        return query.FirstOrDefault();
                    }
                    else
                    {
                        return ObjectSort(query, orderBy, ASC).FirstOrDefault();
                    }
                }
            }
            else
            {
                if (include == null || include.Count == 0)
                {
                    if (orderBy == null)
                    {
                        return dbSet.FirstOrDefault(where);
                    }
                    else
                    {
                        return ObjectSort(dbSet, orderBy, ASC).FirstOrDefault(where);
                    }
                }
                else
                {
                    IQueryable<T> query = dbSet.Include(include[0]);

                    for (int i = 1; i < include.Count; i++)
                    {
                        query = query.Include(include[i]);
                    }

                    if (orderBy == null)
                    {
                        return query.FirstOrDefault(where);
                    }
                    else
                    {
                        return ObjectSort(query, orderBy, ASC).FirstOrDefault(where);
                    }
                }
            }
        }

        public IQueryable<T> GetEntities(Expression<Func<T, bool>> where = null, List<string> include = null, Expression<Func<T, object>> orderBy = null, bool ASC = true, int skip = -1, int take = -1)
        {
            IQueryable<T> query = dbSet.AsQueryable();

            if (where == null)
            {
                if (include == null || include.Count == 0)
                {
                    if (orderBy == null)
                    {
                        return query;
                    }
                    else
                    {
                        if (skip == -1 && take == -1)
                        {
                            return ObjectSort(query, orderBy, ASC);
                        }
                        else if (skip == -1)
                        {
                            return ObjectSort(query, orderBy, ASC).Take(take);
                        }
                        else if (take == -1)
                        {
                            return ObjectSort(query, orderBy, ASC).Skip(skip);
                        }
                        else
                        {
                            return ObjectSort(query, orderBy, ASC).Skip(skip).Take(take);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < include.Count; i++)
                    {
                        query = query.Include(include[i]);
                    }

                    if (orderBy == null)
                    {
                        return query;
                    }
                    else
                    {
                        if (skip == -1 && take == -1)
                        {
                            return ObjectSort(query, orderBy, ASC);
                        }
                        else if (skip == -1)
                        {
                            return ObjectSort(query, orderBy, ASC).Take(take);
                        }
                        else if (take == -1)
                        {
                            return ObjectSort(query, orderBy, ASC).Skip(skip);
                        }
                        else
                        {
                            return ObjectSort(query, orderBy, ASC).Skip(skip).Take(take);
                        }
                    }
                }
            }
            else
            {
                if (include == null || include.Count == 0)
                {
                    if (orderBy == null)
                    {
                        return query.Where(where);
                    }
                    else
                    {
                        if (skip == -1 && take == -1)
                        {
                            query = query.Where(where);
                            return ObjectSort(query, orderBy, ASC);
                        }
                        else if (skip == -1)
                        {
                            query = query.Where(where);
                            return ObjectSort(query, orderBy, ASC).Take(take);
                        }
                        else if (take == -1)
                        {
                            query = query.Where(where);
                            return ObjectSort(query, orderBy, ASC).Skip(skip);
                        }
                        else
                        {
                            query = query.Where(where);
                            return ObjectSort(query, orderBy, ASC).Skip(skip).Take(take);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < include.Count; i++)
                    {
                        query = query.Include(include[i]);
                    }

                    if (orderBy == null)
                    {
                        return query.Where(where);
                    }
                    else
                    {
                        if (skip == -1 && take == -1)
                        {
                            query = query.Where(where);
                            return ObjectSort(query, orderBy, ASC);
                        }
                        else if (skip == -1)
                        {
                            query = query.Where(where);
                            return ObjectSort(query, orderBy, ASC).Take(take);
                        }
                        else if (take == -1)
                        {
                            query = query.Where(where);
                            return ObjectSort(query, orderBy, ASC).Skip(skip);
                        }
                        else
                        {
                            query = query.Where(where);
                            return ObjectSort(query, orderBy, ASC).Skip(skip).Take(take);
                        }
                    }
                }
            }
        }

        public int CountEntities(Expression<Func<T, bool>> where = null, List<string> include = null)
        {
            IQueryable<T> query = dbSet.AsQueryable();

            if (include != null)
            {
                for (int i = 0; i < include.Count; i++)
                {
                    query = query.Include(include[i]);
                }
            }

            if (where == null)
            {
                return query.Count();
            }
            return query.Count(where);
        }

        public bool AnyEntity(Expression<Func<T, bool>> expression, List<string> include = null)
        {
            IQueryable<T> query = dbSet.AsQueryable();
            if (include == null)
            {
                return query.Any(expression);
            }
            for (int i = 0; i < include.Count; i++)
            {
                query.Include(include[i]);
            }
            return query.Any(expression);
        }

        public T AddEntity(T entity)
        {
            T addedEntity = dbSet.Add(entity).Entity;
            db.SaveChanges();
            return addedEntity;
        }

        public void UpdateEntity(T entity, List<string> editedFields)
        {
            T editedEntity = GetFirstEntity(e => e.Id == entity.Id);

            if (editedEntity != null)
            {
                PropertyHelper.Change(typeof(T), editedEntity, entity, editedFields);
            }

            db.SaveChanges();
        }

        public void RemoveEntity(int entityId)
        {
            T removedEntity = GetFirstEntity(e => e.Id == entityId);

            if (removedEntity != null)
            {
                dbSet.Remove(removedEntity);
            }

            db.SaveChanges();
        }

        public void AddRangeEntities(IEnumerable<T> entities)
        {
            dbSet.AddRange(entities);
            db.SaveChanges();
        }

        public void RemoveRangeEntities(IEnumerable<T> entities)
        {
            dbSet.RemoveRange(entities);
            db.SaveChanges();
        }

        #endregion
    }
}
