﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using WorkTestProject.Data.DbModels;

namespace WorkTestProject.Data
{
    public interface IDbContext
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        int SaveChanges();

        DbSet<Product> Products { get; set; }
        DbSet<ProductCategory> ProductCategories { get; set; }
    }
}
