﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace WorkTestProject.Data.Helpers
{
    public static class PropertyHelper
    {
        public static void Change(Type type, object setObject, object getObject, List<string> editedFields)
        {
            PropertyInfo[] fields = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            string[] names = Array.ConvertAll(fields, field => field.Name);

            foreach (string name in names)
            {
                if (editedFields.Contains(name))
                {
                    PropertyInfo prop = type.GetProperty(name);

                    prop.SetValue(setObject, prop.GetValue(getObject));
                }
            }
        }
    }
}
