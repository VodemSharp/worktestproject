﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using WorkTestProject.Data.DbModels.Interfaces;

namespace WorkTestProject.Data.DbModels
{
    public class Product : IDbEntity
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("ProductCategory")]
        public int ProductCategoryId { get; set; }
        public ProductCategory ProductCategory { get; set; }

        public string Name { get; set; }
        public bool IsActive { get; set; }
        public double Price { get; set; }
    }
}
