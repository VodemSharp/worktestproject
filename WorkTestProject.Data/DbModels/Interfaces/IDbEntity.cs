﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorkTestProject.Data.DbModels.Interfaces
{
    public interface IDbEntity
    {
        int Id { get; set; }
    }
}
