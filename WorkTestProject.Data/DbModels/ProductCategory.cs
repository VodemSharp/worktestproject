﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using WorkTestProject.Data.DbModels.Interfaces;

namespace WorkTestProject.Data.DbModels
{
    public class ProductCategory : IDbEntity
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
        public IEnumerable<Product> Products { get; set; }

        public ProductCategory()
        {
            Products = new List<Product> { };
        }
    }
}
