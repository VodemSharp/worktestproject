﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkTestProject.Data.DbModels;

namespace WorkTestProject.Models.Home
{
    public class IndexViewModel
    {
        public List<Product> Products { get; set; }
    }
}
