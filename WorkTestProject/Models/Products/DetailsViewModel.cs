﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkTestProject.Data.DbModels;

namespace WorkTestProject.Models.Products
{
    public class DetailsViewModel
    {
        public Product Product { get; set; }
        public string State { get; set; }
    }
}
