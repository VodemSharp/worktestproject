﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkTestProject.Data.DbModels;
using WorkTestProject.Services.ProductService;

namespace WorkTestProject.Helpers.SeedHelper
{
    public class SeedHelper : ISeedHelper
    {
        private readonly IProductService _productService;

        public SeedHelper(IProductService productService)
        {
            _productService = productService;
        }

        public void Seed()
        {
            _productService.AddProduct(new Product {
                Name = "VISI/pocket",
                IsActive = true,
                Price = 999
            }, "firstClass");

            _productService.AddProduct(new Product
            {
                Name = "VISI/frame",
                IsActive = true,
                Price = 888
            }, "secondClass");
        }
    }
}
