﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkTestProject.Helpers.SeedHelper
{
    public interface ISeedHelper
    {
        void Seed();
    }
}
