﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkTestProject.Models.Home;
using WorkTestProject.Models.Products;

namespace WorkTestProject.Factories.ProductFactory
{
    public interface IProductFactory
    {
        IndexViewModel PrepareIndexViewModel();
        DetailsViewModel PrepareDetailsViewModel(int productId, string state);
    }
}
