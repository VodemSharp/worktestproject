﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkTestProject.Models.Home;
using WorkTestProject.Models.Products;
using WorkTestProject.Services.ProductService;

namespace WorkTestProject.Factories.ProductFactory
{
    public class ProductFactory : IProductFactory
    {
        private readonly IProductService _productService;

        public ProductFactory(IProductService productService)
        {
            _productService = productService;
        }

        public IndexViewModel PrepareIndexViewModel()
        {
            IndexViewModel model = new IndexViewModel
            {
                Products = _productService.ListProducts(null, new List<string> { "ProductCategory" })
            };

            return model;
        }

        public DetailsViewModel PrepareDetailsViewModel(int productId, string state)
        {
            DetailsViewModel model = new DetailsViewModel
            {
                Product = _productService.GetProduct(p => p.Id == productId, new List<string> { "ProductCategory" }),
                State = state
            };

            return model;
        }
    }
}
