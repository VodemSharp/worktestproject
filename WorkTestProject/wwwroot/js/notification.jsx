﻿class Notification extends React.Component {
    render() {
        return (
            <div className="alert alert-success" role="alert">
                <strong>Product updated!</strong> You have successfully updated the product.
            </div>
        );
    }
}

function toNotify(id) {
    ReactDOM.render(
        <Notification />,
        document.getElementById(id)
    );
}