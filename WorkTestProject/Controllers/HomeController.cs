﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WorkTestProject.Factories.ProductFactory;
using WorkTestProject.Helpers.SeedHelper;
using WorkTestProject.Models;
using WorkTestProject.Services.ProductService;

namespace WorkTestProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISeedHelper _seedHelper;
        private readonly IProductFactory _productFactory;

        public HomeController(ISeedHelper seedHelper, IProductFactory productFactory)
        {
            _seedHelper = seedHelper;
            _productFactory = productFactory;
        }

        public IActionResult Index()
        {
            return View(_productFactory.PrepareIndexViewModel());
        }

        [Route("seed")]
        public IActionResult Seed()
        {
            _seedHelper.Seed();
            return RedirectToAction("Index", "Home");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
