﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WorkTestProject.Data.DbModels;
using WorkTestProject.Factories.ProductFactory;
using WorkTestProject.Models.Products;
using WorkTestProject.Services.ProductService;

namespace WorkTestProject.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService _productService;
        private readonly IProductFactory _productFactory;

        public ProductController(IProductService productService, IProductFactory productFactory)
        {
            _productService = productService;
            _productFactory = productFactory;
        }

        [HttpGet]
        [Route("products/{id}/details")]
        public IActionResult Details(int id)
        {
            return View(_productFactory.PrepareDetailsViewModel(id, TempData["notification"]?.ToString()));
        }

        [HttpPost]
        [Route("products/{id}/details")]
        public IActionResult Update(DetailsViewModel model)
        {
            if (String.IsNullOrEmpty(model.Product.Name))
            {
                ModelState.AddModelError("Product.Name", "Product name can not be empty!");
            }

            if (String.IsNullOrEmpty(model.Product.ProductCategory.Name))
            {
                ModelState.AddModelError("Product.ProductCategory.Name", "Category name can not be empty!");
            }

            if (model.Product.Price <= 0)
            {
                ModelState.AddModelError("Product.Price", "Price must be greater than zero!");
            }

            if (!ModelState.IsValid)
            {
                return View("Details", model);
            }

            Product product = _productService.GetProductById(model.Product.Id);
            product.Name = model.Product.Name;
            product.Price = model.Product.Price;
            product.IsActive = model.Product.IsActive;

            _productService.UpdateProduct(model.Product, new List<string> { "Name", "Price", "IsActive" });
            _productService.UpdateCategory(model.Product.Id, model.Product.ProductCategory.Name);

            TempData["notification"] = "updated";
            return RedirectToAction("Update", "Product");
        }
    }
}