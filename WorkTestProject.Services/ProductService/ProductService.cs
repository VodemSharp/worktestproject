﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using WorkTestProject.Data.DbModels;
using WorkTestProject.Data.Repository;

namespace WorkTestProject.Services.ProductService
{
    public class ProductService : IProductService
    {
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<ProductCategory> _productCategoryRepository;

        public ProductService(
            IRepository<Product> productRepository,
            IRepository<ProductCategory> productCategoryRepository
            )
        {
            _productRepository = productRepository;
            _productCategoryRepository = productCategoryRepository;
        }

        #region CRUD

        #region Product

        public Product GetProductById(int id)
        {
            return _productRepository.GetEntityById(id);
        }

        public Product GetProduct(Expression<Func<Product, bool>> expression = null, List<string> include = null, Expression<Func<Product, object>> orderBy = null, bool ASC = true)
        {
            return _productRepository.GetFirstEntity(expression, include, orderBy, ASC);
        }

        public List<Product> ListProducts(Expression<Func<Product, bool>> expression = null, List<string> include = null, Expression<Func<Product, object>> orderBy = null, bool ASC = true, int skip = -1, int take = -1)
        {
            return _productRepository.GetEntities(expression, include, orderBy, ASC, skip, take).ToList();
        }

        public int CountProducts(Expression<Func<Product, bool>> expression = null, List<string> include = null)
        {
            return _productRepository.CountEntities(expression, include);
        }

        public bool AnyProduct(Expression<Func<Product, bool>> expression = null, List<string> include = null)
        {
            return _productRepository.AnyEntity(expression);
        }

        public Product AddProduct(Product product)
        {
            return _productRepository.AddEntity(product);
        }

        public void UpdateProduct(Product product, List<string> editedFields)
        {
            _productRepository.UpdateEntity(product, editedFields);
        }

        public void RemoveProduct(int productId)
        {
            _productRepository.RemoveEntity(productId);
        }

        public void AddRangeProducts(IEnumerable<Product> products)
        {
            _productRepository.AddRangeEntities(products);
        }

        public void RemoveRangeProducts(IEnumerable<Product> products)
        {
            _productRepository.RemoveRangeEntities(products);
        }

        #endregion

        #region ProductCategory

        public ProductCategory GetProductCategoryById(int id)
        {
            return _productCategoryRepository.GetEntityById(id);
        }

        public ProductCategory GetProductCategory(Expression<Func<ProductCategory, bool>> expression = null, List<string> include = null, Expression<Func<ProductCategory, object>> orderBy = null, bool ASC = true)
        {
            return _productCategoryRepository.GetFirstEntity(expression, include, orderBy, ASC);
        }

        public List<ProductCategory> ListProductCategories(Expression<Func<ProductCategory, bool>> expression = null, List<string> include = null, Expression<Func<ProductCategory, object>> orderBy = null, bool ASC = true, int skip = -1, int take = -1)
        {
            return _productCategoryRepository.GetEntities(expression, include, orderBy, ASC, skip, take).ToList();
        }

        public int CountProductCategories(Expression<Func<ProductCategory, bool>> expression = null, List<string> include = null)
        {
            return _productCategoryRepository.CountEntities(expression, include);
        }

        public bool AnyProductCategory(Expression<Func<ProductCategory, bool>> expression = null, List<string> include = null)
        {
            return _productCategoryRepository.AnyEntity(expression);
        }

        public ProductCategory AddProductCategory(ProductCategory productCategory)
        {
            return _productCategoryRepository.AddEntity(productCategory);
        }

        public void UpdateProductCategory(ProductCategory productCategory, List<string> editedFields)
        {
            _productCategoryRepository.UpdateEntity(productCategory, editedFields);
        }

        public void RemoveProductCategory(int productCategoryId)
        {
            _productCategoryRepository.RemoveEntity(productCategoryId);
        }

        public void AddRangeProductCategories(IEnumerable<ProductCategory> productCategories)
        {
            _productCategoryRepository.AddRangeEntities(productCategories);
        }

        public void RemoveRangeProductCategories(IEnumerable<ProductCategory> productCategories)
        {
            _productCategoryRepository.RemoveRangeEntities(productCategories);
        }

        #endregion

        #endregion

        #region Special

        public void AddProduct(Product product, string categoryName)
        {
            if (_productCategoryRepository.AnyEntity(pc => pc.Name == categoryName))
            {
                product.ProductCategoryId = _productCategoryRepository.GetFirstEntity(pc => pc.Name == categoryName).Id;
            }
            else
            {
                product.ProductCategoryId = _productCategoryRepository.AddEntity(new ProductCategory { Name = categoryName }).Id;
            }

            _productRepository.AddEntity(product);
        }

        public void UpdateCategory(int productId, string newCategoryName)
        {
            Product product = _productRepository.GetFirstEntity(p => p.Id == productId);
            ProductCategory productCategory = _productCategoryRepository.GetFirstEntity(pc => pc.Id == product.ProductCategoryId);

            if (productCategory.Name == newCategoryName)
            {
                return;
            }

            if (_productCategoryRepository.AnyEntity(pc => pc.Name == newCategoryName))
            {
                product.ProductCategoryId = _productCategoryRepository.GetFirstEntity(pc => pc.Name == newCategoryName).Id;
            }
            else
            {
                product.ProductCategoryId = _productCategoryRepository.AddEntity(new ProductCategory { Name = newCategoryName }).Id;
            }

            _productRepository.UpdateEntity(product, new List<string> { "ProductCategoryId" });

            if (!_productRepository.AnyEntity(p => p.ProductCategoryId == productCategory.Id))
            {
                _productCategoryRepository.RemoveEntity(productCategory.Id);
            }
        }

        #endregion
    }
}
