﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using WorkTestProject.Data.DbModels;

namespace WorkTestProject.Services.ProductService
{
    public interface IProductService
    {
        #region CRUD

        #region Product

        Product GetProductById(int id);
        Product GetProduct(Expression<Func<Product, bool>> expression = null, List<string> include = null, Expression<Func<Product, object>> orderBy = null, bool ASC = true);
        List<Product> ListProducts(Expression<Func<Product, bool>> expression = null, List<string> include = null, Expression<Func<Product, object>> orderBy = null, bool ASC = true, int skip = -1, int take = -1);
        int CountProducts(Expression<Func<Product, bool>> expression = null, List<string> include = null);
        bool AnyProduct(Expression<Func<Product, bool>> expression = null, List<string> include = null);

        Product AddProduct(Product product);
        void UpdateProduct(Product product, List<string> editedFields);
        void RemoveProduct(int productId);

        void AddRangeProducts(IEnumerable<Product> products);
        void RemoveRangeProducts(IEnumerable<Product> products);

        #endregion

        #region ProductCategory

        ProductCategory GetProductCategoryById(int id);
        ProductCategory GetProductCategory(Expression<Func<ProductCategory, bool>> expression = null, List<string> include = null, Expression<Func<ProductCategory, object>> orderBy = null, bool ASC = true);
        List<ProductCategory> ListProductCategories(Expression<Func<ProductCategory, bool>> expression = null, List<string> include = null, Expression<Func<ProductCategory, object>> orderBy = null, bool ASC = true, int skip = -1, int take = -1);
        int CountProductCategories(Expression<Func<ProductCategory, bool>> expression = null, List<string> include = null);
        bool AnyProductCategory(Expression<Func<ProductCategory, bool>> expression = null, List<string> include = null);

        ProductCategory AddProductCategory(ProductCategory productCategory);
        void UpdateProductCategory(ProductCategory productCategory, List<string> editedFields);
        void RemoveProductCategory(int productCategoryId);

        void AddRangeProductCategories(IEnumerable<ProductCategory> productCategories);
        void RemoveRangeProductCategories(IEnumerable<ProductCategory> productCategories);

        #endregion

        #endregion

        #region Special

        void AddProduct(Product product, string categoryName);
        void UpdateCategory(int productId, string newCategoryName);

        #endregion
    }
}
